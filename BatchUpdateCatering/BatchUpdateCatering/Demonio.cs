﻿using BatchUpdateCatering.Dominio;
using BatchUpdateCatering.Persistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace BatchUpdateCatering
{
    class Demonio
    {
        public static int Main()
        {
            ServicioDAO dao = new ServicioDAO();
            string rutaCola = @".\private$\OutputReservas";
            if (!MessageQueue.Exists(rutaCola))
                MessageQueue.Create(rutaCola);
            MessageQueue cola = new MessageQueue(rutaCola);
            cola.Formatter = new XmlMessageFormatter(new Type[] { typeof(Servicio) });
            int numeroMensajes = cola.GetAllMessages().Count();
            int contador = 0;
            while (contador < numeroMensajes)
            {
                Message mensaje = cola.Receive();
                Servicio servicioEncolado = (Servicio)mensaje.Body;
                contador = contador + 1;
                dao.Actualizar(servicioEncolado.Codigo);
            }
            return contador;
        }
    }
}
