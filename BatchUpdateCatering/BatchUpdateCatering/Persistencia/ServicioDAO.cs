﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatchUpdateCatering.Persistencia
{
    class ServicioDAO
    {
        public void Actualizar(string detalle)
        {
            NpgsqlConnection con = new NpgsqlConnection(ConexionUtil.Cadena);
            con.Open();
            //NpgsqlDataAdapter ada = new NpgsqlDataAdapter("update reservas set cat_confirmado = true where cat_id = id_cat", con);
            NpgsqlCommand cmd = new NpgsqlCommand("update reservas set cat_confirmado = true where cat_id = @id_cat", con);
            cmd.Parameters.Add(new NpgsqlParameter("@id_cat", detalle));
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
